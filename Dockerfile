FROM alpine:latest

RUN apk --no-cache add python3 py3-pip

RUN pip3 install flask flask-jsonpify flask-restful

WORKDIR /python_api

COPY ./python-api.py .

ENTRYPOINT ["python3", "python-api.py"]
